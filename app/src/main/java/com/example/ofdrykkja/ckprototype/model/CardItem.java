package com.example.ofdrykkja.ckprototype.model;

/**
 * Created by IntelliJ IDEA.
 * Author: a.kiryukhin
 * Date: 7/28/17
 */

public class CardItem {

    private final String text;
    private final int color;

    //TODO: add color annotation
    public CardItem(String text, int color) {
        this.text = text;
        this.color = color;
    }

    public String getText() {
        return text;
    }

    //TODO: add color annotation
    public int getColor() {
        return color;
    }

}
