package com.example.ofdrykkja.ckprototype.utils;

import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.support.annotation.ColorInt;
import android.support.annotation.ColorRes;

/**
 * Created by IntelliJ IDEA.
 * Author: a.kiryukhin
 * Date: 7/28/17
 */

public class MaterialColorRandomGenerator {

    private final Context context;

    public MaterialColorRandomGenerator(Context context) {
        this.context = context;
    }

    //TODO: use constant for mdcolor
    //TODO: add color annotation
    public int get(String typeColor) {
        int returnColor = Color.BLACK;
        final Resources resources = context.getResources();
        final int arrayId = resources.getIdentifier(
                "mdcolor_" + typeColor,
                "array",
                context.getApplicationContext().getPackageName()
        );
        if (arrayId != 0) {
            final TypedArray colors = resources.obtainTypedArray(arrayId);
            final int index = (int) (Math.random() * colors.length());
            returnColor = colors.getColor(index, Color.BLACK);
            colors.recycle();
        }
        return returnColor;
    }

}
