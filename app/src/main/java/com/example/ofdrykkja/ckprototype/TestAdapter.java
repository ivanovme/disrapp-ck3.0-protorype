package com.example.ofdrykkja.ckprototype;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * Author: a.kiryukhin
 * Date: 7/25/17
 */

public class TestAdapter extends RecyclerView.Adapter<TestAdapter.ViewHolder> {

    private final List<String> data;

    public TestAdapter(List<String> data) {
        this.data = data;
    }

    @Override
    public TestAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(
                LayoutInflater.from(parent.getContext()).inflate(
                        android.R.layout.simple_list_item_1,
                        parent,
                        false
                )
        );
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.textView.setText(data.get(position));
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        public final TextView textView;

        public ViewHolder(TextView textView) {
            super(textView);
            this.textView = textView;
        }

        public ViewHolder(View textView) {
            super(textView);
            this.textView = (TextView) textView;
        }

    }

}
