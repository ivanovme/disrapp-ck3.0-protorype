package com.example.ofdrykkja.ckprototype.model;

import org.parceler.Parcel;

/**
 * Created by IntelliJ IDEA.
 * Author: a.kiryukhin
 * Date: 7/27/17
 */

@Parcel
public class Category {

    int id;

    public Category() {
    }

    public Category(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

}
