package com.example.ofdrykkja.ckprototype.categories;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.ofdrykkja.ckprototype.R;
import com.example.ofdrykkja.ckprototype.model.Category;

import org.parceler.Parcels;

import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by IntelliJ IDEA.
 * Author: a.kiryukhin
 * Date: 7/28/17
 */

public class CategoryFragment extends Fragment {

    private static final String ARG_CATEGORY = "category";

    @BindView(R.id.category_title)
    TextView transactionTitle;

    public CategoryFragment() {
    }

    public static CategoryFragment newInstance(Category category) {
        final Bundle bundle = new Bundle(1);
        bundle.putParcelable(ARG_CATEGORY, Parcels.wrap(category));
        final CategoryFragment fragment = new CategoryFragment();
        fragment.setArguments(bundle);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(
            LayoutInflater inflater,
            @Nullable ViewGroup container,
            @Nullable Bundle savedInstanceState
    ) {
        final View view = inflater.inflate(
                R.layout.fragment_category,
                container,
                false
        );
        ButterKnife.bind(this, view);
        final Category category = Parcels.unwrap(
                getArguments().getParcelable(ARG_CATEGORY)
        );
        transactionTitle.setText(
                String.format(Locale.ENGLISH, "category #%d", category.getId())
        );
        return view;
    }

}
