package com.example.ofdrykkja.ckprototype;

import android.content.ClipData;
import android.content.ClipDescription;
import android.support.constraint.ConstraintLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.DragEvent;
import android.view.View;
import android.widget.Button;

import com.example.ofdrykkja.ckprototype.cards.CardViewPagerAdapter;
import com.example.ofdrykkja.ckprototype.cards.DepthPageTransformer;
import com.example.ofdrykkja.ckprototype.categories.CategoryAdapter;
import com.example.ofdrykkja.ckprototype.categories.CategoryFragment;
import com.example.ofdrykkja.ckprototype.categories.ImageDragShadowBuilder;
import com.example.ofdrykkja.ckprototype.categories.SmoothScrollGridLayoutManager;
import com.example.ofdrykkja.ckprototype.model.CardItem;
import com.example.ofdrykkja.ckprototype.model.Category;
import com.example.ofdrykkja.ckprototype.utils.MaterialColorRandomGenerator;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = MainActivity.class.getName();
    //    @BindView(R.id.view_pager)
//    com.example.ofdrykkja.ckprototype.vertical.VerticalViewPager viewPager;
    @BindView(R.id.statistics_list)
    RecyclerView statisticsList;
    @BindView(R.id.feed_list)
    RecyclerView feedList;
    @BindView(R.id.card_view_pager)
    ViewPager cardViewPager;
    @BindView(R.id.categories_list)
    RecyclerView categoriesList;
    @BindView(R.id.categories_container)
    ConstraintLayout categoriesContainer;
    @BindView(R.id.consumption_button)
    Button consumptionButton;
    @BindView(R.id.income_button)
    Button incomeButton;
    @BindView(R.id.main_scroll)
    MainScrollView mainScroll;

    private View lastTargetCategory;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        setupMainScroll();
        setupStatistics();
        setupCardsView();
        setupFeed();
    }

    private void setupMainScroll() {
        mainScroll.setScreenSelectedListener(screenIndex -> setButtonsVisibility(
                screenIndex != MainScrollView.SCREEN_INDEX_BOTTOM
        ));
    }

    private void setupCardsView() {
        setupCards(cardViewPager, 10, 4);
        setupCategoriesList(categoriesList);
        setupDragButton(categoriesContainer, consumptionButton, incomeButton);
    }

    private void setupStatistics() {
        statisticsList.setLayoutManager(new LinearLayoutManager(this));
        statisticsList.addItemDecoration(
                new DividerItemDecoration(this, DividerItemDecoration.VERTICAL)
        );
        final List<String> items = new ArrayList<>();
        for (int i = 0; i < 100; i++) {
            items.add(String.format(Locale.ENGLISH, "statistic item: %d", i));
        }
        statisticsList.setAdapter(new TestAdapter(items));
    }

    private void setupFeed() {
        feedList.setLayoutManager(new LinearLayoutManager(this));
        feedList.addItemDecoration(
                new DividerItemDecoration(this, DividerItemDecoration.VERTICAL)
        );
        final List<String> items = new ArrayList<>();
        for (int i = 0; i < 100; i++) {
            items.add(String.format(Locale.ENGLISH, "feed item: %d", i));
        }
        feedList.setAdapter(new TestAdapter(items));
    }

    private void setupCards(
            final ViewPager cardViewPager,
            final int cardsCount,
            final int visibleCount
    ) {
        final CardViewPagerAdapter adapter = new CardViewPagerAdapter(getSupportFragmentManager());
        final MaterialColorRandomGenerator colorGenerator = new MaterialColorRandomGenerator(this);
        for (int i = 0; i < cardsCount; i++) {
            if (i == 0) {
                adapter.addItem(
                        new CardItem(
                                String.format(Locale.ENGLISH, "special card: %d", i),
                                android.R.color.white
                        )
                );
            } else if (i == 9) {
                adapter.addItem(
                        new CardItem(
                                String.format(Locale.ENGLISH, "special card: %d", i),
                                android.R.color.white
                        )
                );
            } else {
                adapter.addItem(
                        new CardItem(
                                String.format(Locale.ENGLISH, "card: %d", i),
                                colorGenerator.get("300")
                        )
                );
            }
        }
        cardViewPager.setAdapter(adapter);
        cardViewPager.setPageTransformer(true, new DepthPageTransformer());
        cardViewPager.setOffscreenPageLimit(visibleCount);
        cardViewPager.setCurrentItem(Integer.MAX_VALUE / 2, false);
        cardViewPager.addOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {
            @Override
            public void onPageSelected(final int position) {
                final int realPos = position % cardsCount;
                if (realPos == 0 || realPos == cardsCount - 1) {
                    setButtonsVisibility(false);
                } else {
                    setButtonsVisibility(true);
                }
            }
        });
    }

    private void setupDragButton(final View dropView, final Button... buttons) {
        for (final Button button : buttons) {
            button.setTag("coin_view");
            button.setOnLongClickListener(v -> {
                dropView.setVisibility(View.VISIBLE);
                for (final Button b : buttons) {
                    b.setAlpha(0);
                    b.setAlpha(0);
                }
                final ClipData.Item item = new ClipData.Item((CharSequence) v.getTag());
                final ClipData dragData = new ClipData(
                        v.getTag().toString(),
                        new String[]{ClipDescription.MIMETYPE_TEXT_PLAIN},
                        item
                );
                v.startDrag(
                        dragData,
                        ImageDragShadowBuilder.fromResource(this, R.drawable.bg_coin),
                        null,
                        0
                );
                return true;
            });
            button.setOnDragListener((v, dragEvent) -> {
                switch (dragEvent.getAction()) {
                    case DragEvent.ACTION_DRAG_ENDED:
                        Log.d(TAG, "[ended]");
                        dropView.setVisibility(View.GONE);
                        for (final Button b : buttons) {
                            b.setAlpha(1);
                            b.setAlpha(1);
                        }
                        break;
                }
                return true;
            });
        }
    }

    private void setupCategoriesList(final RecyclerView categoriesList) {
        final List<Category> categories = new ArrayList<>();
        final int totalCategoriesCount = 102;
        for (int t = 0; t < totalCategoriesCount; t++) {
            categories.add(new Category(t));
        }
        final CategoryAdapter categoriesAdapter = new CategoryAdapter(categories);
        final int spanCount = 3;
        categoriesList.setLayoutManager(
                new SmoothScrollGridLayoutManager(
                        this,
                        spanCount,
                        GridLayoutManager.HORIZONTAL,
                        false
                )
        );
        categoriesList.setAdapter(categoriesAdapter);
        categoriesList.setHasFixedSize(true);
        categoriesList.addItemDecoration(
                new CategoryAdapter.ItemDecoration(
                        spanCount,
                        getResources().getDimensionPixelSize(R.dimen.transaction_space)
                )
        );
        categoriesList.setItemViewCacheSize(20);
        categoriesList.setDrawingCacheEnabled(true);
        categoriesList.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);
        final DisplayMetrics metrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(metrics);
        final int coinSize = getResources().getDimensionPixelSize(R.dimen.coin_size);
        final int coinMargin = getResources().getDimensionPixelSize(R.dimen.transaction_space);
        final int totalCoinWidth = coinSize + coinMargin;
//        ((SimpleItemAnimator) categoriesList.getItemAnimator()).setSupportsChangeAnimations(false);
        categoriesList.setItemAnimator(null);
        categoriesList.setOnDragListener((v, dragEvent) -> {
            final View targetCategory = categoriesList.findChildViewUnder(
                    dragEvent.getX(),
                    dragEvent.getY()
            );

            final int targetCategoryIndex = categoriesList.getChildAdapterPosition(
                    targetCategory
            );

            final int lastTargetCategoryIndex = lastTargetCategory == null ? -1 : categoriesList.getChildAdapterPosition(lastTargetCategory);

            switch (dragEvent.getAction()) {
                case DragEvent.ACTION_DRAG_ENTERED:
                case DragEvent.ACTION_DRAG_LOCATION:
                    Log.d(TAG, "ACTION_DRAG_ENTERED || ACTION_DRAG_LOCATION");
                    if (dragEvent.getX() >= metrics.widthPixels - (totalCoinWidth / 2)) {
                        categoriesList.smoothScrollToPosition(totalCategoriesCount - 1);
                    } else if (dragEvent.getX() <= (totalCoinWidth / 2)) {
                        categoriesList.smoothScrollToPosition(0);
                    } else {
                        categoriesList.stopScroll();
                    }
                    if (targetCategory != null && !targetCategory.isSelected()) {
                        targetCategory.setHovered(true);
                        if (lastTargetCategory != null && lastTargetCategoryIndex != -1 && lastTargetCategoryIndex != targetCategoryIndex) {
                            //TODO fix fast scroll and check visibility on Screen.
                            lastTargetCategory.setHovered(false);
                            lastTargetCategory = targetCategory;
                        } else {
                            lastTargetCategory = targetCategory;
                        }
                    }
                    Log.d(TAG, "[onDrag] targetCategoryIndex: " + targetCategoryIndex);
                    break;
                case DragEvent.ACTION_DROP:
                    if (targetCategoryIndex > -1) {
                        openCategory(categories.get(targetCategoryIndex));
                    }
                    if (lastTargetCategory != null)
                        lastTargetCategory.setHovered(false);
                    break;
                case DragEvent.ACTION_DRAG_EXITED:
                    Log.d(TAG, "ACTION_DRAG_EXITED");
                    categoriesList.stopScroll();
                    if (lastTargetCategory != null)
                        lastTargetCategory.setHovered(false);
                    break;
            }
            return true;
        });
    }

    private void openCategory(final Category category) {
        getSupportFragmentManager()
                .beginTransaction()
                .replace(android.R.id.content, CategoryFragment.newInstance(category))
                .addToBackStack(null)
                .commit();
    }

    public final void setButtonsVisibility(final boolean visible) {
        if (visible) {
            incomeButton.setVisibility(View.VISIBLE);
            consumptionButton.setVisibility(View.VISIBLE);
        } else {
            incomeButton.setVisibility(View.INVISIBLE);
            consumptionButton.setVisibility(View.INVISIBLE);
        }
    }

}
