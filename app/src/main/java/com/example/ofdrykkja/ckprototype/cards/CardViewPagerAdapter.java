package com.example.ofdrykkja.ckprototype.cards;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.example.ofdrykkja.ckprototype.model.CardItem;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * Author: a.kiryukhin
 * Date: 7/26/17
 */

public class CardViewPagerAdapter extends FragmentPagerAdapter {

    private final List<CardItem> items = new ArrayList<>();

    public CardViewPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    public void addItem(CardItem cardItem) {
        items.add(cardItem);
    }

    @Override
    public Fragment getItem(int position) {
//        return items.get(position);
//        return items.get(position % items.size());
        return CardItemFragment.newInstance(
                items.get(position % items.size()).getText(),
                items.get(position % items.size()).getColor()
        );
    }

/*    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        super.destroyItem(container, position, object);
        CardItemFragment cf = (CardItemFragment) object;
        mFragMan.beginTransaction().remove(cf).commit();
        mFrags.add(position, ContentFragment.newInstance(cf.getmParam1()));
        mFrags.remove(position+1);
    }*/

    @Override
    public int getCount() {
//        return items.size();
        return Integer.MAX_VALUE;
    }

}
