package com.example.ofdrykkja.ckprototype;

import android.content.Context;
import android.content.res.TypedArray;
import android.support.annotation.IntDef;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.util.Log;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * Created by IntelliJ IDEA.
 * Author: a.kiryukhin
 * Date: 8/1/17
 */

public class MainScrollView extends NestedScrollView {

    private static final String TAG = MainScrollView.class.getName();
    private static final int TOTAL_SCREEN_COUNT = 3;
    @Retention(RetentionPolicy.SOURCE)
    @IntDef({SCREEN_INDEX_TOP, SCREEN_INDEX_CENTER, SCREEN_INDEX_BOTTOM})
    public @interface ScreenIndex {
    }

    private List<View> screens = new ArrayList<>(TOTAL_SCREEN_COUNT);
    public static final int SCREEN_INDEX_TOP = 0;
    public static final int SCREEN_INDEX_CENTER = 1;
    public static final int SCREEN_INDEX_BOTTOM = 2;
    private static final int SWIPE_MIN_DISTANCE = 50;
    private static final int SWIPE_THRESHOLD_VELOCITY = 300;
    private GestureDetector gestureDetector;
    private boolean touchDispatched;
    private List<ItemInfo> itemHeights = new ArrayList<>();
    private ScreenSelectedListener screenSelectedListener;

    public MainScrollView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);
    }

    public MainScrollView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context, attrs);
    }

    private void init(final Context context, final AttributeSet attrs) {
        for (int i = 0; i < TOTAL_SCREEN_COUNT; i++) {
            itemHeights.add(new ItemInfo(0, 0, 0));
        }
        final TypedArray a = context.getTheme().obtainStyledAttributes(
                attrs,
                R.styleable.MainScrollView,
                0,
                0
        );
        int topScreenLayout;
        int centerScreenLayout;
        int bottomScreenLayout;
        try {
            topScreenLayout = a.getResourceId(
                    R.styleable.MainScrollView_topScreen,
                    0
            );
            centerScreenLayout = a.getResourceId(
                    R.styleable.MainScrollView_centerScreen,
                    0
            );
            bottomScreenLayout = a.getResourceId(
                    R.styleable.MainScrollView_bottomScreen,
                    0
            );
        } finally {
            a.recycle();
        }
        final LinearLayout wrapperLinearLayout = new LinearLayout(context);
        wrapperLinearLayout.setOrientation(LinearLayout.VERTICAL);
        addView(
                wrapperLinearLayout,
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT
        );
        final LayoutInflater inflater = LayoutInflater.from(context);
        screens.add(inflater.inflate(topScreenLayout, wrapperLinearLayout));
        screens.add(inflater.inflate(centerScreenLayout, wrapperLinearLayout));
        screens.add(inflater.inflate(bottomScreenLayout, wrapperLinearLayout));
        gestureDetector = new GestureDetector(context, new MainGestureDetector());
    }

    public void setScreenSelectedListener(ScreenSelectedListener listener) {
        this.screenSelectedListener = listener;
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        super.dispatchTouchEvent(ev);
        touchDispatched = gestureDetector.onTouchEvent(ev);
        return touchDispatched;
    }

    @Override
    public boolean onTouchEvent(MotionEvent ev) {
        Log.d(TAG, "[onTouch] motionEvent: " + ev);
        if (touchDispatched) {
            Log.d(TAG, "[onTouch] gesture confirmed");
            return true;
        } else if (
                ev.getAction() == MotionEvent.ACTION_UP ||
                        ev.getAction() == MotionEvent.ACTION_CANCEL
                ) {
            switch (getActiveScreenIndex()) {
                case SCREEN_INDEX_TOP:
                    Log.d(TAG, "[onTouch] scroll to top");
                    scrollToTop();
                    break;
                case SCREEN_INDEX_CENTER:
                    Log.d(TAG, "[onTouch] scroll to center");
                    scrollToCenter();
                    break;
                case SCREEN_INDEX_BOTTOM:
                    Log.d(TAG, "[onTouch] scroll to down");
                    scrollToBottom();
                    break;
            }
            return true;
        } else {
            return super.onTouchEvent(ev);
        }
    }

    private void scrollToTop() {
        scrollTo(0, 0);
        if (screenSelectedListener != null) {
            screenSelectedListener.onScreenSelected(SCREEN_INDEX_TOP);
        }
    }

    private void scrollToBottom() {
        scrollTo(0, getMaxVerticalScroll() - getMeasuredHeight());
        if (screenSelectedListener != null) {
            screenSelectedListener.onScreenSelected(SCREEN_INDEX_BOTTOM);
        }
    }

    private void scrollToCenter() {
        scrollTo(0, (getMaxVerticalScroll() - getMeasuredHeight()) / 2);
        if (screenSelectedListener != null) {
            screenSelectedListener.onScreenSelected(SCREEN_INDEX_CENTER);
        }
    }

    @ScreenIndex
    private int getActiveScreenIndex() {
        if (getScrollY() < itemHeights.get(SCREEN_INDEX_TOP).getBottom() / 2) {
            return SCREEN_INDEX_TOP;
        } else if (getScrollYFromBottom() < itemHeights.get(SCREEN_INDEX_BOTTOM).getHeight() / 2) {
            return SCREEN_INDEX_BOTTOM;
        } else {
            return SCREEN_INDEX_CENTER;
        }
    }

    private int getScrollYFromBottom() {
        return getMaxVerticalScroll() - (getMeasuredHeight() + getScrollY());
    }

    private int getMaxVerticalScroll() {
        return itemHeights.get(SCREEN_INDEX_BOTTOM).getBottom();
    }

    @Override
    protected void onLayout(boolean changed, int l, int t, int r, int b) {
        super.onLayout(changed, l, t, r, b);
        final LinearLayout wrapper = (LinearLayout) getChildAt(0);
        for (int i = 0; i < TOTAL_SCREEN_COUNT; i++) {
            final View child = wrapper.getChildAt(i);
            itemHeights.get(i).setTop(child.getTop());
            itemHeights.get(i).setBottom(child.getBottom());
            itemHeights.get(i).setHeight(child.getMeasuredHeight());
        }
    }

    private boolean isScreenShouldScroll(
            @ScreenIndex final int screenIndex,
            final MotionEvent startEvent
    ) {
        final View view = ((ViewGroup) getChildAt(0)).getChildAt(screenIndex);
        if (view instanceof RecyclerView) {
            switch (screenIndex) {
                case SCREEN_INDEX_TOP:
                    if (recyclerViewScrolledToBottom((RecyclerView) view)) {
                        return false;
                    }
                    break;
                case SCREEN_INDEX_BOTTOM:
                    if (recyclerViewScrolledToTop((RecyclerView) view)) {
                        return false;
                    }
                    break;
                case SCREEN_INDEX_CENTER:
                    break;
            }
        }
        return eventInView(startEvent, view);
    }

    private static boolean eventInView(final MotionEvent event, final View view) {
        final int location[] = new int[2];
        view.getLocationOnScreen(location);
        final int viewX = location[0];
        final int viewY = location[1];
        return (
                (event.getX() > viewX && event.getX() < (viewX + view.getWidth())) &&
                        (event.getY() > viewY && event.getY() < (viewY + view.getHeight()))
        );
    }

    private static boolean recyclerViewScrolledToBottom(RecyclerView recyclerView) {
        return (
                ((LinearLayoutManager) recyclerView.getLayoutManager()).findLastCompletelyVisibleItemPosition() ==
                        recyclerView.getAdapter().getItemCount() - 1
        );
    }

    private static boolean recyclerViewScrolledToTop(RecyclerView recyclerView) {
        return ((LinearLayoutManager) recyclerView.getLayoutManager()).findFirstCompletelyVisibleItemPosition() == 0;
    }

    private class MainGestureDetector extends GestureDetector.SimpleOnGestureListener {

        private boolean isFlingDown(final MotionEvent e1, final MotionEvent e2, float velocityY) {
            return e1.getY() - e2.getY() > SWIPE_MIN_DISTANCE && Math.abs(velocityY) > SWIPE_THRESHOLD_VELOCITY;
        }

        private boolean isFlingUp(final MotionEvent e1, final MotionEvent e2, float velocityY) {
            return e2.getY() - e1.getY() > SWIPE_MIN_DISTANCE && Math.abs(velocityY) > SWIPE_THRESHOLD_VELOCITY;
        }

        @Override
        public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
            Log.d(TAG, "[onFling] e1: " + e1);
            Log.d(TAG, "[onFling] e2: " + e2);
            Log.d(TAG, "[onFling] velocity: " + velocityY);
            if (e1 == null || e2 == null) {
                Log.d(TAG, "[onFling] wrong events");
                return false;
            }
            Log.d(TAG, "[onFling][screen_scroll_dbg] is top screen: "  + isScreenShouldScroll(SCREEN_INDEX_TOP, e1));
            Log.d(TAG, "[onFling][screen_scroll_dbg] is center screen: "  + isScreenShouldScroll(SCREEN_INDEX_CENTER, e1));
            Log.d(TAG, "[onFling][screen_scroll_dbg] is bottom screen: "  + isScreenShouldScroll(SCREEN_INDEX_BOTTOM, e1));
            if (isScreenShouldScroll(SCREEN_INDEX_TOP, e1) || isScreenShouldScroll(SCREEN_INDEX_BOTTOM, e1)) {
                return false;
            }
            if (isFlingDown(e1, e2, velocityY)) {
                Log.d(TAG, "[onFling] list scroll down");
                switch (getActiveScreenIndex()) {
                    case SCREEN_INDEX_TOP:
                        Log.d(TAG, "[onFling] scroll to center");
                        scrollToCenter();
                        break;
                    case SCREEN_INDEX_CENTER:
                        Log.d(TAG, "[onFling] scroll to bottom");
                        scrollToBottom();
                        break;
                    case SCREEN_INDEX_BOTTOM:
                        break;
                    default:
                        return false;
                }
                return true;
            } else if (isFlingUp(e1, e2, velocityY)) {
                Log.d(TAG, "[onFling] list scroll up");
                switch (getActiveScreenIndex()) {
                    case SCREEN_INDEX_BOTTOM:
                        Log.d(TAG, "[onFling] scroll to center");
                        scrollToCenter();
                        break;
                    case SCREEN_INDEX_CENTER:
                        Log.d(TAG, "[onFling] scroll to top");
                        scrollToTop();
                        break;
                    case SCREEN_INDEX_TOP:
                        break;
                    default:
                        return false;
                }
                return true;
            }
            return false;
        }

        @Override
        public boolean onDown(MotionEvent e) {
            Log.d(TAG, "[onDown] e: " + e);
            return true;
        }

        @Override
        public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY) {
            Log.d(TAG, "[onScroll]");
            return false;
        }
    }

    private static class ItemInfo {

        private int height;
        private int top;
        private int bottom;

        ItemInfo(int height, int top, int bottom) {
            this.height = height;
            this.top = top;
            this.bottom = bottom;
        }

        int getTop() {
            return top;
        }

        void setTop(int top) {
            this.top = top;
        }

        int getBottom() {
            return bottom;
        }

        void setBottom(int bottom) {
            this.bottom = bottom;
        }

        @Override
        public String toString() {
            return String.format(
                    Locale.ENGLISH,
                    "{top: %d; bottom: %d; height: %d}",
                    top,
                    bottom,
                    height
            );
        }

        int getHeight() {
            return height;
        }

        void setHeight(int height) {
            this.height = height;
        }
    }

    public interface ScreenSelectedListener {
        void onScreenSelected(@ScreenIndex int screenIndex);
    }
}