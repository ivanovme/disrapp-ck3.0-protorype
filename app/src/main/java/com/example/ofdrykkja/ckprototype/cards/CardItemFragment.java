package com.example.ofdrykkja.ckprototype.cards;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.ofdrykkja.ckprototype.R;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by IntelliJ IDEA.
 * Author: a.kiryukhin
 * Date: 7/26/17
 */

public class CardItemFragment extends Fragment {

    private static final String CARD_TEXT = "card_text";
    private static final String CARD_COLOR = "card_color";

    @BindView(R.id.text)
    TextView text;
    @BindView(R.id.card_body)
    RelativeLayout card;

    public CardItemFragment() {
    }

    //TODO: add color annotation
    public static CardItemFragment newInstance(String cardText, int cardColor) {
        final Bundle bundle = new Bundle(2);
        final CardItemFragment fragment = new CardItemFragment();
        bundle.putString(CARD_TEXT, cardText);
        bundle.putInt(CARD_COLOR, cardColor);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(
            LayoutInflater inflater,
            @Nullable ViewGroup container,
            @Nullable Bundle savedInstanceState
    ) {
        final View view = inflater.inflate(R.layout.view_card, container, false);
        ButterKnife.bind(this, view);
        text.setText(getArguments().getString(CARD_TEXT));
        card.setBackgroundColor(getArguments().getInt(CARD_COLOR));
        return view;
    }

}
