package com.example.ofdrykkja.ckprototype.categories;

import android.graphics.Rect;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.ofdrykkja.ckprototype.R;
import com.example.ofdrykkja.ckprototype.model.Category;

import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * Author: a.kiryukhin
 * Date: 7/27/17
 */

public class CategoryAdapter extends RecyclerView.Adapter<CategoryAdapter.ViewHolder> {

    private static final String TAG = CategoryAdapter.class.getName();
    private final List<Category> data;

    public CategoryAdapter(List<Category> data) {
        this.data = data;
        setHasStableIds(true);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(
                R.layout.view_category, parent, false
        ));
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Log.d(TAG, "[onBindViewHolder] position: " + position);
        holder.view.setText(String.valueOf(position));
    }

    @Override
    public long getItemId(int position) {
        return data.get(position).getId();
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {

        private final TextView view;

        public ViewHolder(View view) {
            super(view);
            this.view = (TextView) view;
        }

    }

    public static class ItemDecoration extends RecyclerView.ItemDecoration {

        private final int spanCount;
        private final int spacing;

        public ItemDecoration(int spanCount, int spacing) {
            this.spanCount = spanCount;
            this.spacing = spacing;
        }

        @Override
        public void getItemOffsets(
                Rect outRect,
                View view,
                RecyclerView parent,
                RecyclerView.State state
        ) {
            outRect.left = spacing;
            outRect.right = spacing;
            outRect.bottom = spacing;
            if (parent.getChildLayoutPosition(view) < spanCount) {
                outRect.left = spacing;
            } else {
                outRect.left = 0;
            }
        }

    }

}
