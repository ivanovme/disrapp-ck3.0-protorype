package com.example.ofdrykkja.ckprototype.cards;

import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.View;

/**
 * Created by IntelliJ IDEA.
 * Author: a.kiryukhin
 * Date: 7/26/17
 */

public class DepthPageTransformer implements ViewPager.PageTransformer {

    private static final String TAG = DepthPageTransformer.class.getName();

    @Override
    public void transformPage(View view, float position) {
        Log.d(TAG,"[transformPage] position: " + position);
        if (position < -1) {
            view.setAlpha(0);
        } else if (position <= 0) {
            view.setAlpha(1);
            view.setTranslationX(0);
        } else if (position > 0) {
            view.setAlpha(1);
            view.setTranslationX(-view.getWidth() * position);
            view.setTranslationY((view.getHeight() / 50) * -position);
        } else {
            view.setAlpha(1);
        }
    }

}
